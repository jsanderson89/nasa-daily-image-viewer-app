package com.HeadFirstAndroidDevelopment.nasadailyimage;

import android.graphics.Bitmap;

public interface DownloadImageTaskResponse {
	void processFinish(Bitmap output);
}
