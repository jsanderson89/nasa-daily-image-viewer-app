package com.HeadFirstAndroidDevelopment.nasadailyimage;

import java.io.InputStream;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.HttpEntity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	
	public DownloadImageTaskResponse delegate=null;
	
	
	@Override
	protected Bitmap doInBackground(String... params)
	{
		return downloadBitmap(params[0]);
	}
	
	@Override
	protected void onPostExecute(Bitmap bitmap)
	{
		if(isCancelled())
		{
			bitmap = null;
		}
		
		delegate.processFinish(bitmap);
	}
	
	private Bitmap downloadBitmap(String url)
	{
		System.out.println("starting download...");
		final AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
		final HttpGet getRequest = new HttpGet(url);
		
		try
		{
			HttpResponse response = client.execute(getRequest);
			
			final int statusCode = response.getStatusLine().getStatusCode();
			
			if (statusCode != HttpStatus.SC_OK)
			{
				Log.w("ImageDownloader", "Error " + statusCode + "while retrieving bitmap from " + url);
				return null;
			}
		
			final HttpEntity entity = response.getEntity();
			if (entity != null)
			{
				InputStream inputStream = null;
				try
				{
					inputStream = entity.getContent();
					final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
					return bitmap;
				}
				
				finally
				{
					if (inputStream != null)
					{
						inputStream.close();
					}
					
					entity.consumeContent();
				}
			}
		}
		
		catch (IOException e)
		{
			getRequest.abort();
			Log.w("ImageDownloader", "IOException while retrieving bitmap from " + url, e);
		}
		
		catch (IllegalStateException e)
		{
			getRequest.abort();
			Log.w("ImageDownloader", "IllegalStateException while retrieving bitmap from " + url,e);
		}
		
		catch (Exception e)
		{
			getRequest.abort();
			Log.w("ImageDownloader", "Error while retrieving bitmap from " + url,e);
		}
		
		finally
		{
			if (client != null)
			{
				client.close();
			}
		}
		
		return null;
	
	}

}

