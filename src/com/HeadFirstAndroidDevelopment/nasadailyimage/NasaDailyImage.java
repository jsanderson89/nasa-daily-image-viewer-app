package com.HeadFirstAndroidDevelopment.nasadailyimage;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

public class NasaDailyImage extends Activity implements ArrayDownloadTaskResponse {

	private List<DailyImage> dailyImage;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nasa_daily_image);
        System.out.println("onCreate"); 
        //getDailyImageFromFile();
        getDailyImageFromWeb();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nasa_daily_image, menu);
        return true;
    }
    
    public void getDailyImageFromFile()
    {
      	//file input
		InputStream xmlInput;
		
		try {
				xmlInput = getAssets().open("nasaimageTwo.xml");
				readStream(xmlInput);
			}
		
		catch (FileNotFoundException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("file not found");
			}
		
		catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("ioexception");
			}
    }
   
    
    public void getDailyImageFromWeb()
    {
    	URL url = null;
		try 
		{
			url = new URL("http://www.nasa.gov/rss/dyn/image_of_the_day.rss");
			//url = new URL("http://www.google.com");
		} 
		
		catch (MalformedURLException e) 
		{
			System.out.println("MalformedURLException");
		}
		
		inputTask(url);
    }
    
    
    public void inputTask(URL url)
    {
    	HttpURLConnection connection = null;
    	InputStream webInput = null;
    	
    	try
		{
			connection = (HttpURLConnection) url.openConnection();
			webInput = new BufferedInputStream(connection.getInputStream());
		}
		
		catch (IOException e)
		{
			//e.printStackTrace();
			System.out.println("ioexception");
			System.out.println(e);
		}
		
		finally
		{
			System.out.println("FINALLY");
			readStream(webInput);
			connection.disconnect();
			
		}
    }
    
    public void readStream(InputStream in)
    {
    	//DailyImage dImage = new DailyImage();
    	
    	System.out.println("readStream");
		Parser parser = new Parser();
        
		try 
        {
			parser.parse(in);
		}
        
        catch (IOException e) 
        {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try
		{
			dailyImage = parser.dailyImageData;
			ArrayDownloadTask task = new ArrayDownloadTask();
			task.delegate = this;
			task.execute(dailyImage);
		}
		
		catch (NullPointerException e)
		{
			System.out.println("null");
		}

    }
    
    public void arrayDownloadTaskResponse()
    {
    	displayInfo();
	}
    
    private void displayInfo() throws NullPointerException 
    {
    	Log.w("displayInfo", "entered");
    	
    	int imageToDisplay = 1;

    	TextView title = (TextView)findViewById(R.id.viewTitle);
    	System.out.println(this.dailyImage.get(imageToDisplay).getTitle());
    	title.setText(this.dailyImage.get(imageToDisplay).getTitle());
    	
    	TextView date = (TextView)findViewById(R.id.viewDate);
    	System.out.println(this.dailyImage.get(imageToDisplay).getDate());
    	date.setText(this.dailyImage.get(imageToDisplay).getDate());
    	
    	TextView description = (TextView)findViewById(R.id.viewDescription);
    	System.out.println(this.dailyImage.get(imageToDisplay).getDescription());
    	description.setText(this.dailyImage.get(imageToDisplay).getDescription());
    	
    	ImageView image = (ImageView)findViewById(R.id.viewImage);
    	Bitmap testImage = this.dailyImage.get(imageToDisplay).getImage();
    	System.out.println(testImage);
    	image.setAdjustViewBounds(true);
    	image.setImageBitmap(testImage);

    }
}
