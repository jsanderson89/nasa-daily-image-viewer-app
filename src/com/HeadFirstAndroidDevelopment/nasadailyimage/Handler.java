package com.HeadFirstAndroidDevelopment.nasadailyimage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Handler extends DefaultHandler{
	
	private boolean parseTitleTag = false;
	private boolean parseDescriptionTag = false;
	private boolean parseDateTag = false;
	private boolean parseImageUrlTag = false;
	
	private String urlAttribute;
	
	StringBuilder elementText = new StringBuilder();
	
	protected DailyImage dImage = new DailyImage();
	protected List<DailyImage> dImageList = new ArrayList<DailyImage>();
	
	
	public void startDocument() throws SAXException
	{
		System.out.println("**********Start of Doc**********");
	}
	
	public void endDocument() throws SAXException
	{
		System.out.println("**********End of Doc**********");
	}
	
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
	{
		System.out.println("Start Element:" + qName);
		
		if (qName.equalsIgnoreCase("item"))
		{
			dImage = new DailyImage();
		}
		
		if(qName.equalsIgnoreCase("title")) 
		{
			parseTitleTag = true;
		}
		
		if(qName.equalsIgnoreCase("description")) 
		{
			parseDescriptionTag = true;
		}
		
		if(qName.equalsIgnoreCase("pubDate")) 
		{
			parseDateTag = true;
		}
		
		if(qName.equalsIgnoreCase("enclosure")) 
		{
			parseImageUrlTag = true;
			
			//if(attributes.getValue("type") == "image/jpeg")
			//{
				urlAttribute = attributes.getValue("url");
			//}
		}
		
	}
	
	public void endElement(String uri, String localName, String qName) throws SAXException
	{
		System.out.println("End Element :" + qName);
		
		if(qName.equalsIgnoreCase("title")) 
		{
			String title = elementText.toString();
			System.out.println("title: " + title);
			dImage.setTitle(title);
			
			parseTitleTag = false;
		}
		
		if(qName.equalsIgnoreCase("description")) 
		{
			
			String description = elementText.toString();
			System.out.println("Description: " + description);
			dImage.setDescription(description);
			
			parseDescriptionTag = false;
		}
		
		if(qName.equalsIgnoreCase("pubDate")) 
		{
			String date = elementText.toString();
			System.out.println("Date: " + date);
			dImage.setDate(date);
			
			parseDateTag = false;
		}
		
		if(qName.equalsIgnoreCase("enclosure")) 
		{
			System.out.println("Url");
			dImage.setImageUrl(urlAttribute);
			urlAttribute = null;
			parseImageUrlTag = false;
		}	
		
		elementText = new StringBuilder();
		
		if (qName.equalsIgnoreCase("item"))
		{
			System.out.println("DailyImage:");
			System.out.println(dImage.getTitle());
			System.out.println(dImage.getDescription());
			System.out.println(dImage.getDate());
			
			if(dImage.getImageUrl() != null)
				System.out.println(dImage.getImageUrl());
			
			try
			{
				dImageList.add(dImage);
			}

			catch (NullPointerException e)
			{
				System.out.println("null");
			}
					
			
			dImage = new DailyImage();
			
			//breaks after first element
			//throw new TerminateParsingException("I'm so sorry");
		}
	}
	
	public void characters (char ch[], int start, int length) throws TerminateParsingException, SAXException
	{
		
		elementText.append(ch, start, length);
	}
	
	public List<DailyImage> getImageData()
	{
		return dImageList;
	}

}


