package com.HeadFirstAndroidDevelopment.nasadailyimage;

import org.xml.sax.SAXException;

@SuppressWarnings("serial")
public class TerminateParsingException extends SAXException {
	public TerminateParsingException() {
		// TODO Auto-generated constructor stub;
	}
	
	public TerminateParsingException(String message) {
		// TODO Auto-generated constructor stub
		super(message);
	}
	
	public String getMessage()
    {
        return super.getMessage();
    }
}
