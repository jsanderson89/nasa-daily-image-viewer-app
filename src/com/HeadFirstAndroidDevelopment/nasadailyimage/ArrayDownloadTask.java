package com.HeadFirstAndroidDevelopment.nasadailyimage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import android.os.AsyncTask;

public class ArrayDownloadTask extends AsyncTask<List<DailyImage>, Float, Void> 
{
	public ArrayDownloadTaskResponse delegate = null;
	
	@Override
	protected Void doInBackground(List<DailyImage>... arg0) 
	{
		for (int i=0; i < arg0[0].size(); i++)
		{
			try 
			{
				arg0[0].get(i).downloadImage();
			} 
			
			catch (MalformedURLException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Void v)
	{
		delegate.arrayDownloadTaskResponse();
	}
}
