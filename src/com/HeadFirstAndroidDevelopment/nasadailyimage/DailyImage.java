package com.HeadFirstAndroidDevelopment.nasadailyimage;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.util.Log;

public class DailyImage implements DownloadImageTaskResponse{
	
	private String title;
	private String description;
	private String date;
	private String imageUrl;
	private Bitmap image;
	
	
	public DailyImage()
	{
		//default constructor
		
	}
	
	public DailyImage(String title, String description, String date, String imageUrl)
	{
		this.title = title;
		this.description = description;
		this.date = date;
		this.imageUrl = imageUrl;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public void setTitle(String inTitle)
	{
		this.title = inTitle;
	}
	
	
	public String getDescription()
	{
		return description;
	}
	
	public void setDescription(String inDescription)
	{
		this.description = inDescription;
	}
	
	
	public String getDate()
	{
		return date;
	}
	
	public void setDate(String inDate)
	{
		this.date = inDate;
	}
	
	
	public String getImageUrl()
	{
		return imageUrl;
	}
	
	public void setImageUrl(String inUrl)
	{
		this.imageUrl = inUrl;
		
		//this.imageUrl = "http://www.nasa.gov/sites/default/files/styles/946xvariable_height/public/gigapan_small_str01.png";
	}
	
	public Bitmap getImage()
	{
		return image;
	}
	
	public void setImage(Bitmap inImage)
	{
		this.image = inImage;
	}
	
	/*
	public void downloadImage() throws IOException, MalformedURLException
	{
		String url = getImageUrl();
		
		DownloadImageTask task = new DownloadImageTask();
		
		task.delegate = this;
		task.execute(url);
		
	}
	*/
	
	public void downloadImage() throws IOException, MalformedURLException
	{
		System.out.println("starting download...");
		final AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
		System.out.println(this.imageUrl);
		final HttpGet getRequest = new HttpGet(this.imageUrl);
		
		
		try
		{
			HttpResponse response = client.execute(getRequest);
			
			final int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK)
			{
				Log.w("ImageDownloader", "Error " + statusCode + "while retrieving bitmap from " + imageUrl);
			}
		
			
			final HttpEntity entity = response.getEntity();
			if (entity != null)
			{
				InputStream inputStream = null;
				try
				{
					inputStream = entity.getContent();
					final Bitmap bitmap = decodeFile(inputStream);
					//final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
					
					this.setImage(bitmap);
				}
				
				finally
				{
					if (inputStream != null)
					{
						inputStream.close();
					}
					
					entity.consumeContent();
				}
			}
		}
		
		catch (FileNotFoundException e)
		{
			getRequest.abort();
			Log.w("ImageDownloader", "FileNotFoundException while retrieving bitmap from " + imageUrl, e);
		}
		
		catch (IOException e)
		{
			getRequest.abort();
			Log.w("ImageDownloader", "IOException while retrieving bitmap from " + imageUrl, e);
		}
		
		catch (IllegalStateException e)
		{
			getRequest.abort();
			Log.w("ImageDownloader", "IllegalStateException while retrieving bitmap from " + imageUrl,e);
		}
		
		catch (Exception e)
		{
			getRequest.abort();
			Log.w("ImageDownloader", "Error while retrieving bitmap from " + imageUrl,e);
		}
		
		finally
		{
			if (client != null)
			{
				System.out.println("finally client close");
				client.close();
			}
		}
	}

	private Bitmap decodeFile(InputStream istream) throws FileNotFoundException, IOException
	{
		BufferedInputStream bstream = new BufferedInputStream(istream);
		int requiredSize = 200;
		
		if (bstream.markSupported() == true)
		{
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(bstream, null, options);
			
			bstream.reset();
			
			int scale = 1;
			while(options.outWidth/scale/2 >= requiredSize &&  options.outHeight/scale/2>=requiredSize)
			{
		           scale*=2;
			}
				
			BitmapFactory.Options optionsTwo = new BitmapFactory.Options();
			optionsTwo.inSampleSize = scale;
			return BitmapFactory.decodeStream(bstream, null, optionsTwo);
		}
		
		else
		{
			Log.w("decodeFile", "mark not supported");
			return null;
		}
	}
	
	public void processFinish(Bitmap output)
	{
		this.setImage(output);
		Log.w("ProcessFinish", "Process complete. Image is " + getImage());
	}
	
	
 
}
