package com.HeadFirstAndroidDevelopment.nasadailyimage;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

public class Parser {

	//private String parseTitle;
	//private String parseDescription;
	//private String parseDate;
	//private String parseImageUrl;
	
	public List<DailyImage> dailyImageData = new ArrayList<DailyImage>();
	
	//public void parse(InputStream is) throws IOException
	public void parse(InputStream is) throws IOException
	{
		System.out.println("parse");
		
		try
		{
			//obtain and configure SAX parser
			SAXParserFactory factory = SAXParserFactory.newInstance();
			
			//obtain object for SAX parser
			SAXParser parser = factory.newSAXParser();
			
			//default handler for SAX handler class
			Handler handler = new Handler();
			
			parser.parse(is, handler);
			
			dailyImageData = handler.getImageData();
			System.out.println(handler.getImageData());
			
			//all three methods are written in handler's body
		}

		catch (ParserConfigurationException e) 
		{
			// TODO Auto-generated catch block
			System.out.println("ParserConfigurationException");
			e.printStackTrace();
		}
		
		catch (TerminateParsingException e)
		{
			System.out.println("*******************************************");
			System.out.println("terminate parsing exception caught");
				
			if(e.getMessage() != null)
				System.out.println(e.getMessage());
			else
				System.out.println("message is null");
		}
		
		catch (SAXException e) 
		{
			System.out.println(e);

			// TODO Auto-generated catch block
			System.out.println("sax exception");
			//e.printStackTrace();
		}

	}
}
