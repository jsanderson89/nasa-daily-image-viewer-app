Nasa Daily Image viewer
Version 1


This is a simple Android test app which reads the information from the Nasa 
"Image of the Day" RSS feed (http://www.nasa.gov/rss/dyn/image_of_the_day.rss),
then downloads and displays it.

This project is based on an excercise from the book "Head First Android
Development." Only the first few chapters were made available before the book was
cancelled, and there were numerous problems with the book's code. Using only the
book's reccomended layout, I instead created a new, more elegant solution to the
problem being adressed.   


Issues:
	-No progress bar during initial download. UI will not hang, but it will
	 change from  the placeholder info as soon as the data is downloaded.

	-Displays only the most recent Daily Image. Though this was the initial
	 goal of the program, I also download the other recent images and their
	 data. In the next version I plan to include the ability to display
	 them all.

	-Images are displayed off-center on the UI. I intend to redesign the UI
	 for the next version.


Copyright Jesse Anderson, 2014.